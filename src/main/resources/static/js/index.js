let form = document.getElementById("form");
let text = document.getElementById("text");
let submitButton = document.getElementById("submitButton");

submitButton.style.visibility = "hidden"

text.oninput = (e) => {
    if (e.target.value === ""){
        submitButton.style.visibility = "hidden"
    }
    else {
        submitButton.style.visibility = "visible"
    }
}

form.addEventListener("keydown", function(event) {
    if (event.key === "Enter" && text.value === "") {
        event.preventDefault();
    }
})