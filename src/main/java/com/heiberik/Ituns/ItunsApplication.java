package com.heiberik.Ituns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItunsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItunsApplication.class, args);
	}
}
