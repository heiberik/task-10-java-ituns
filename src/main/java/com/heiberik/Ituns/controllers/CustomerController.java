package com.heiberik.Ituns.controllers;

import com.heiberik.Ituns.data_access.CustomerData;
import com.heiberik.Ituns.data_access.CustomerPopularGenre;
import com.heiberik.Ituns.models.query_models.CustomerSpending;
import com.heiberik.Ituns.data_access.CustomersByCountryData;
import com.heiberik.Ituns.models.Customer;
import com.heiberik.Ituns.data_access.CustomerSpendingData;
import com.heiberik.Ituns.models.query_models.CustomersByCountry;
import com.heiberik.Ituns.models.query_models.GenreTotal;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

// endpoints for the Customer api.
@CrossOrigin
@RestController
public class CustomerController {

    // Task 1. Return all customers.
    @GetMapping("/api/customers")
    public ArrayList<Customer> getAllCustomers() {
        return CustomerData.getAllCustomers();
    }

    // Task 2. Add a new customer.
    @PostMapping("/api/customers")
    public ResponseEntity newCustomer(@RequestBody Customer newCustomer){
        boolean didWork = CustomerData.addNewCustomer(newCustomer);
        if (didWork){
            return new ResponseEntity(HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Task 3. Update customer.
    @PutMapping("/api/customers/{id}")
    public ResponseEntity updateCustomer(@PathVariable String id, @RequestBody Customer updatedCustomer) {
        boolean didWork =  CustomerData.updateCustomer(updatedCustomer, id);
        if (didWork){
            return new ResponseEntity(HttpStatus.OK);
        }
        else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Task 4. Get customers by country.
    @GetMapping("/api/customers/countries/top")
    public ArrayList<CustomersByCountry> getCustomersByCountries() {
        return CustomersByCountryData.getCustomersByCountry();
    }

    // Task 5. Get top spenders.
    @GetMapping("/api/customers/spenders/top")
    public ArrayList<CustomerSpending> getCustomersByHighestSpenders() {
        return CustomerSpendingData.getCustomerSpendings();
    }

    // Task 6. Get customers most popular genre.
    @GetMapping("/api/customers/{id}/popular/genre")
    public ArrayList<GenreTotal> getSpecificCustomersMostPopularGenre(@PathVariable String id) {
        return CustomerPopularGenre.getMostPopularGenre(id);
    }


}
