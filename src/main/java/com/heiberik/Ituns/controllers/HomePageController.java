package com.heiberik.Ituns.controllers;

import com.heiberik.Ituns.data_access.ArtistData;
import com.heiberik.Ituns.data_access.GenreData;
import com.heiberik.Ituns.data_access.TrackData;
import com.heiberik.Ituns.data_access.TrackExtendedInfoData;
import com.heiberik.Ituns.models.Search;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;

import javax.validation.Valid;

@Controller
public class HomePageController {

    @GetMapping("/")
    public String homepage(Model model) {
        model.addAttribute("artists", ArtistData.getRandomArtists());
        model.addAttribute("tracks", TrackData.getRandomTracks());
        model.addAttribute("genres", GenreData.getRandomGenres());
        model.addAttribute("search", new Search());
        return "homepage";
    }

    @GetMapping("/search")
    public String searchpage(@Valid Search search, Errors errors, Model model) {
        if (errors != null && errors.getErrorCount() > 0) {
            model.addAttribute("message", "Search cannot be empty");
            return "errorpage";
        } else {
            model.addAttribute("search", search);
            model.addAttribute("tracks", TrackExtendedInfoData.getTrack(search.getQuery()));
            return "searchpage";
        }
    }
}
