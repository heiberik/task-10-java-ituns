package com.heiberik.Ituns.data_access;

import com.heiberik.Ituns.models.Customer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

// class responsible for querying the database and return java objects.
public class CustomerData {

    private static String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection conn = null;

    // method for updating a customer.
    public static boolean updateCustomer(Customer updatedCustomer, String id){
        try{

            String query = "UPDATE customer";
            query += " SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?";
            query += " WHERE CustomerId = ?";

            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement(query);
            preparedStatement.setString(1, updatedCustomer.getFirstName());
            preparedStatement.setString(2, updatedCustomer.getLastName());
            preparedStatement.setString(3, updatedCustomer.getCountry());
            preparedStatement.setString(4, updatedCustomer.getPostalCode());
            preparedStatement.setString(5, updatedCustomer.getPhoneNumber());
            preparedStatement.setString(6, id);

            preparedStatement.executeUpdate();
            return true;

        }
        catch (Exception exception){
            System.out.println(exception.toString());
            return false;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
    }

    // add a new customer.
    public static boolean addNewCustomer(Customer newCustomer){

        try{

            String query = "INSERT INTO customer(FirstName, LastName, Email, Country, PostalCode, Phone, SupportRepId)";
            query += " VALUES(?,?,?,?,?,?,?)";

            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement(query);
            preparedStatement.setString(1, newCustomer.getFirstName());
            preparedStatement.setString(2, newCustomer.getLastName());
            preparedStatement.setString(3, "hardcoded_email");
            preparedStatement.setString(4, newCustomer.getCountry());
            preparedStatement.setString(5, newCustomer.getPostalCode());
            preparedStatement.setString(6, newCustomer.getPhoneNumber());
            preparedStatement.setString(7, "1");


            preparedStatement.executeUpdate();
            return true;

        }
        catch (Exception exception){
            System.out.println(exception.toString());
            return false;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
    }

    // return all customers in db.
    public static ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone FROM customer");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return customers;
    }

}
