package com.heiberik.Ituns.data_access;

import com.heiberik.Ituns.models.query_models.GenreTotal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

// class responsible for querying the database and return java objects.
public class CustomerPopularGenre {

    private static String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection conn = null;

    // method that returns a customers favourite genres.
    public static ArrayList<GenreTotal> getMostPopularGenre(String id){

        ArrayList<GenreTotal> genres = new ArrayList<>();

        try{

            conn = DriverManager.getConnection(URL);

            // find the max number
            String query = "SELECT genre.GenreId, genre.Name as GenreName, Count(genre.Name) as totalNumber";
            query += " FROM customer NATURAL JOIN invoice NATURAL JOIN invoiceline";
            query += " INNER JOIN track on invoiceline.TrackId = track.TrackId";
            query += " INNER JOIN genre on track.GenreId = genre.GenreId";
            query += " WHERE CustomerId = ?";
            query += " GROUP BY genre.GenreId";
            query += " ORDER BY Count(GenreName) DESC";
            query += " LIMIT 1";

            PreparedStatement preparedStatement =
                    conn.prepareStatement(query);

            preparedStatement.setString(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            int highest = 0;
            while (resultSet.next()) {
                highest = resultSet.getInt("totalNumber");
            }

            // find all genres having max.
            query = "SELECT genre.GenreId, genre.Name as GenreName, Count(genre.Name) as totalNumber";
            query += " FROM customer NATURAL JOIN invoice NATURAL JOIN invoiceline";
            query += " INNER JOIN track on invoiceline.TrackId = track.TrackId";
            query += " INNER JOIN genre on track.GenreId = genre.GenreId";
            query += " WHERE CustomerId = ?";
            query += " GROUP BY genre.GenreId";
            query += " HAVING totalNumber = ?";
            query += " ORDER BY Count(GenreName) DESC";

            preparedStatement =
                    conn.prepareStatement(query);

            preparedStatement.setString(1, id);
            preparedStatement.setInt(2, highest);

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                genres.add(
                        new GenreTotal(
                                resultSet.getString("GenreId"),
                                resultSet.getString("GenreName"),
                                resultSet.getInt("totalNumber")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return genres;
    }
}
