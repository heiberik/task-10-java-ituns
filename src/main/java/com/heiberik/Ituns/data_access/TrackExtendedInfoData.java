package com.heiberik.Ituns.data_access;

import com.heiberik.Ituns.models.query_models.TrackExtendedInfo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

// class responsible for querying the database and return java objects.
public class TrackExtendedInfoData {

    private static String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection conn = null;

    // get information about all tracks that matches the query.
    public static ArrayList<TrackExtendedInfo> getTrack(String name){

        ArrayList<TrackExtendedInfo> tracks = new ArrayList<>();

        try{

            String query = "SELECT track.Name as trackName, artist.Name as artistName, album.Title as albumTitle, genre.Name as genreName";
            query += " FROM track NATURAL JOIN album INNER JOIN artist on album.ArtistId = artist.ArtistID INNER JOIN genre on genre.GenreId = track.GenreId";
            query += " WHERE UPPER(track.Name) LIKE ('%"+name+"%')";
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(
                        new TrackExtendedInfo(
                                resultSet.getString("trackName"),
                                resultSet.getString("artistName"),
                                resultSet.getString("albumTitle"),
                                resultSet.getString("genreName")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return tracks;
    }
}
