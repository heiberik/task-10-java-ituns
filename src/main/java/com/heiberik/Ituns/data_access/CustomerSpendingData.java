package com.heiberik.Ituns.data_access;

import com.heiberik.Ituns.models.query_models.CustomerSpending;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

// class responsible for querying the database and return java objects.
public class CustomerSpendingData {

    private static String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection conn = null;

    // get spending by each customer in descending order.
    public static ArrayList<CustomerSpending> getCustomerSpendings(){
        ArrayList<CustomerSpending> spendings = new ArrayList<>();

        try{

            String query = "SELECT CustomerId, FirstName, LastName, SUM(Total) as totalSum";
            query += " FROM customer NATURAL JOIN invoice GROUP BY CustomerId";
            query += " ORDER BY totalSum DESC";

            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                spendings.add(
                        new CustomerSpending(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getDouble("totalSum")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return spendings;
    }
}
