package com.heiberik.Ituns.data_access;

import com.heiberik.Ituns.models.query_models.CustomersByCountry;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

// class responsible for querying the database and return java objects.
public class CustomersByCountryData {

    private static String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection conn = null;

    // method that returns number of customers per country in descending order.
    public static ArrayList<CustomersByCountry> getCustomersByCountry(){
        ArrayList<CustomersByCountry> customers = new ArrayList<>();

        try{

            String query = "SELECT Country, Count(CustomerId) as numberOfCustomers";
            query += " FROM customer GROUP BY Country";
            query += " ORDER BY Count(CustomerId) DESC";

            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new CustomersByCountry(
                                resultSet.getString("Country"),
                                resultSet.getInt("numberOfCustomers")

                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return customers;
    }
}
