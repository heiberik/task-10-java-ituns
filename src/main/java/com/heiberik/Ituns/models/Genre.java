package com.heiberik.Ituns.models;

public class Genre {

    private String genreID;
    private String name;

    public Genre(String genreID, String name) {
        this.genreID = genreID;
        this.name = name;
    }

    public String getGenreID() {
        return genreID;
    }

    public String getName() {
        return name;
    }
}
