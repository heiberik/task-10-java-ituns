package com.heiberik.Ituns.models;

public class Track {

    private String trackID;
    private String Name;

    public Track(String trackID, String name) {
        this.trackID = trackID;
        Name = name;
    }

    public String getTrackID() {
        return trackID;
    }

    public String getName() {
        return Name;
    }
}
