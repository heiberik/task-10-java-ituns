package com.heiberik.Ituns.models.query_models;

public class CustomersByCountry {

    private String country;
    private int numberOfCustomers;


    public CustomersByCountry(String country, int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
        this.country = country;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
