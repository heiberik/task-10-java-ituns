package com.heiberik.Ituns.models.query_models;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CustomerSpending {

    private String customerId;
    private String customerFirstName;
    private String customerLastName;
    private double spending;

    public CustomerSpending(String customerId, String customerFirstName, String customerLastName, double spending) {
        this.customerId = customerId;
        this.customerFirstName = customerFirstName;
        this.customerLastName = customerLastName;

        BigDecimal bd = BigDecimal.valueOf(spending);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        this.spending =  bd.doubleValue();
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public double getSpending() {
        return spending;
    }

}
