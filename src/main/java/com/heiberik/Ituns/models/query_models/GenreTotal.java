package com.heiberik.Ituns.models.query_models;

public class GenreTotal {

    private String genreID;
    private String name;

    private int total;

    public GenreTotal(String genreID, String name, int total) {
        this.genreID = genreID;
        this.name = name;
        this.total = total;
    }

    public String getGenreID() {
        return genreID;
    }

    public String getName() {
        return name;
    }

    public int getTotal() {
        return total;
    }
}
