package com.heiberik.Ituns.models;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class Search {

    // Prevent empty search queries.
    @NotNull(message = "Cannot be empty")
    @NotEmpty(message = "Cannot be empty")
    private String query;

    public String getQuery(){
        return query;
    }

    public void setQuery(String query){
        this.query = query;
    }
}
