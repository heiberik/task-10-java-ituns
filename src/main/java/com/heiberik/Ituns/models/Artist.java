package com.heiberik.Ituns.models;

public class Artist {

    private String artistID;
    private String name;

    public Artist(String artistID, String name) {
        this.artistID = artistID;
        this.name = name;
    }

    public String getArtistID() {
        return artistID;
    }

    public String getName() {
        return name;
    }
}
