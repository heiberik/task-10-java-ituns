# Task 10 - Java - Ituns

REST endpoints with Spring. Serving html with Thymeleaf.

App hosted at Heroku: https://ituns-noroff.herokuapp.com/.

#### Endpoints
Endpoint  | Type
------------- | -------------
"/"                                 | (GET)
"/search"                           | (GET)
"/api/customers"                    | (GET)
"/api/customers"                    | (POST)
"/api/customers/:id"                | (PUT)
"/api/customers/countries/top"      | (GET)
"/api/customers/spenders/top"       | (GET)
"/api/customers/{id}/popular/genre" | (GET)